# README #

This repository will get you up and running with the basics of a geospatial dataset running across SOLR and Jena Fuseki. This uses a set of data included in the repository, which gets pushed into a Jena TDB triple store. The SOLR instance is configured with a second collection (or core, the terms seem interchangeable). The TDB then gets queried and the geospatial aspect of the data is pushed across into this SOLR instance.

Because of the dependencies, lots of stuff is included in the repo which wouldn't normally be, ubt in the interests of acting as a quick-start they make life easier. 

### A Vagrant script along with puppet to get jena and solr spatial running ###

* Quick summary

* Version
0.1 This is not production ready code, your mileage may (and most likely will) vary. Comments and suggestions are more than welcome!

### Dependencies ###

* Git -  in order to clone the repo, though in theory you can download it all as a zip from here https://bitbucket.org/TimCoatesNHS/spatial_vms/get/master.zip .
* Vagrant - this whole project is based around Vagrant managing the creation of two Virtual Machines.
* Virtualbox - the current build of this targets creating Virtual Machines in VirtualBox. This can be modified to use other virtual machine providers, but you're on your own with getting that working I'm afraid.

### Usage ###

* Clone this repo
* Change into the resulting spatial_vms directory
* vagrant up
* Wait and wait, as this will take some time, and depends on a LOT of downloading (particularly to install java).

### What you get ###

* Data loaded into TDB, with a spatial component passed across into SOLR
* Fuseki understanding it's data has a spatial aspect to it and knowing where to call SOLR.
* SOLR running on http://192.168.33.5:8983/solr/ and also available on http://localhost:8983/solr/
* Fuseki running on http://192.168.33.6:3030/ and also available on http://localhost:3030/

### TODO ###

* Find a more efficient way of getting Java onto both machines
* Compress the 'assets' files better.
* Add Elda onto the 'setup' possibly on the SOLR machine. Elda references Fuseki over http, so putting it on the SOLR machine 'illustrates' this fact, but could possibly confuse the issue in other ways?