# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

############## First we build the SOLR machine ###############
  config.vm.define "solr" do |solr|
    solr.vm.box = 'trusty64'
    solr.vm.box_url = 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
    solr.vm.network :private_network, ip: "192.168.33.5"
    solr.vm.hostname = "solr.dev.net"

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine. In the example below,
    # accessing "localhost:8080" will access port 80 on the guest machine.
    solr.vm.network :forwarded_port, guest: 22, host: 2022
    solr.vm.network :forwarded_port, guest: 8983, host: 8983

    # The following block configures the Virtualbox specifics
    solr.vm.provider :virtualbox do |vb|
      # Don't boot with headless mode
      vb.gui = true
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
      vb.customize ["modifyvm", :id, "--vram", "2"]
    end

    # The following block configures the puppet provisioning
    solr.vm.provision :puppet do |puppet|
      puppet.manifests_path = "manifests"
      puppet.manifest_file  = "solr.pp"
    end

  end

############## Now the JENA machine ###############
  config.vm.define "jena" do |jena|
    jena.vm.box = 'trusty64'
    jena.vm.box_url = 'https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box'
    jena.vm.network :private_network, ip: "192.168.33.6"
    jena.vm.hostname = "jena.dev.net"

    # Create a forwarded port mapping which allows access to a specific port
    # within the machine from a port on the host machine. In the example below,
    # accessing "localhost:8080" will access port 80 on the guest machine.
    # jena.vm.network :forwarded_port, guest: 80, host: 8080
    jena.vm.network :forwarded_port, guest: 22, host: 2033
    jena.vm.network :forwarded_port, guest: 3030, host: 3030

    # The following block configures the Virtualbox specifics
    jena.vm.provider :virtualbox do |vb|
      # Don't boot with headless mode
      vb.gui = true
      vb.customize ["modifyvm", :id, "--memory", "1024"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
      vb.customize ["modifyvm", :id, "--vram", "2"]
    end

    # The following block configures the puppet provisioning
    jena.vm.provision :puppet do |puppet|
      puppet.manifests_path = "manifests"
      puppet.manifest_file  = "jena.pp"
    end

  end

end
