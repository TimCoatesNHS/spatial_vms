  file { '/etc/motd':
	  content => "

      Jena server
      - Version 1.1
      - OS:     trusty-server-cloudimg-amd64
      - IP:     $ipaddr
    \n\n"
  }

# Update the latest ubuntu packages
  exec { 'update':
    command => '/usr/bin/apt-get update',
  }

# Install git
  package { "git-core":
    ensure => "latest",
    require => Exec["update"],
  }

# Install unzip
  package { "unzip":
    ensure => "latest",
    require => Package["git-core"],
  }

# Install curl
  package { "curl":
    ensure => "latest",
    require => Package["unzip"],
  }

# Install Java jre
  exec { 'java':
    command => '/bin/bash /vagrant/Assets/equip_java8.sh',
    timeout => 420,
    require => Package["curl"],
  }

# Open fuseki
  exec { "unpack-fuseki":
    command => "/bin/tar zxf /vagrant/Assets/jena/jena-fuseki-1.1.0-distribution.tar.gz",
    require => Package["unzip"]
  }

# Open jena
  exec { "unpack-jena":
    command => "/bin/tar zxf /vagrant/Assets/jena/apache-jena-2.12.0.tar.gz",
    require => Package["unzip"],
  }

# Set ownership of jena
  file { "own-jena":
    path => "/home/vagrant/apache-jena-2.12.0",
    owner  => "vagrant",
    group  => "vagrant",
    mode   => 777,
    require => Exec["unpack-jena"],
  }

# Set ownership of fuseki
  file { "own-fuseki":
    path => "/home/vagrant/jena-fuseki-1.1.0",
    owner  => "vagrant",
    group  => "vagrant",
    mode   => 777,
    require => Exec["unpack-fuseki"],
  }

# Create DB folder
  file { "make-tdb-folder":
    path => "/var/DB",
    ensure => "directory",
    owner  => "vagrant",
    group  => "vagrant",
    mode   => 777,
    require => Exec["unpack-jena"],
    before => Exec["make-tdb"]
  }

# Create TDB
  exec { "make-tdb":
    command => "/home/vagrant/apache-jena-2.12.0/bin/tdbloader --loc=/var/DB /vagrant/Assets/jena/NHSUKChoice.nt",
    user  => "vagrant",
    group  => "vagrant",
    require => Exec["java"],
  }

# Set fuseki's config location
  exec { "set-fuseki-conf":
    command => "/usr/bin/sudo cat /vagrant/Assets/jena/FUSEKI_CONF.txt >> /etc/environment",
    before => Exec["start-fuseki"],
  }

# Copy fuseki config file over
  file { "copy-fuseki-conf":
    path => "/home/vagrant/jena-fuseki-1.1.0/config.ttl",
    source => "/vagrant/Assets/jena/config.ttl",
    owner => "vagrant",
    group => "vagrant",
    require => Exec["make-tdb"],
  }

# Copy the assembler file across
  file { "copy-spatial-assembler":
    path => "/home/vagrant/jena-fuseki-1.1.0/spatial_assembler.ttl",
    source => "/vagrant/Assets/jena/spatial_assembler.ttl",
    owner => "vagrant",
    group => "vagrant",
    mode => "666",
    require => File["copy-fuseki-conf"],
  }

  file { "copy-jena-spatial":
    path => "/home/vagrant/jena-fuseki-1.1.0/jena-spatial-1.0.2.jar",
    source => "/vagrant/Assets/jena/jena-spatial-1.0.2.jar",
    owner => "vagrant",
    group => "vagrant",
    mode => "777",
    require => File["copy-spatial-assembler"],
  }

  exec { "make-spatial":
    command => "/usr/bin/java -cp /home/vagrant/jena-fuseki-1.1.0/jena-spatial-1.0.2.jar:/home/vagrant/apache-jena-2.12.0/lib/*:/vagrant/Assets/solr/solrwar/* jena.spatialindexer --desc=/vagrant/Assets/jena/spatial_assembler.ttl",
    user => "vagrant",
    group => "vagrant",
    cwd => "/home/vagrant/jena-fuseki-1.1.0",
    require => File["copy-jena-spatial"],
  }
#######################

# Start fuseki
  exec { "start-fuseki":
    environment => ["FUSEKI_CONF=/home/vagrant/jena-fuseki-1.1.0/config.ttl"],
    command => "/home/vagrant/jena-fuseki-1.1.0/fuseki start",
    user => "vagrant",
    group => "vagrant",
    cwd => "/home/vagrant/jena-fuseki-1.1.0",
    require => Exec["make-spatial"],
  }

# Tell the user we're ready
  notify { 'ready':
    message  => "Fuseki is now running at http://192.168.33.6:3030/",
    require => Exec["start-fuseki"],
  }