  file { '/etc/motd':
  	content => "

      SOLR server
      - Version 1.1
      - OS:     trusty-server-cloudimg-amd64
      - IP:     $ipaddr
    \n\n"
  }

# Update the latest ubuntu packages
  exec { 'update':
    command => '/usr/bin/apt-get update',
  }

# Install git
  package { "git-core":
    ensure => "latest",
    require => Exec["update"],
  }

# Install unzip
  package { "unzip":
    ensure => "latest",
    require => Package["git-core"],
  }

# Install curl
  package { "curl":
    ensure => "latest",
    require => Package["unzip"],
  }

# Install Java jre
  exec { 'java':
    command => '/bin/bash /vagrant/Assets/equip_java8.sh',
    timeout => 420,
    require => Package["curl"],
}

# Open solr
  exec { "unpack-solr":
    command => "/bin/tar zxf /vagrant/Assets/solr/solr-4.10.1.tgz",
    require => Package["unzip"],
}

# chmod solr-4.10.1
  file { "own-solr":
    path => "/home/vagrant/solr-4.10.1",
    ensure => directory,
    recurse => true,
    owner => "vagrant",
    group => "vagrant",
    mode => "777",
    require => Exec["unpack-solr"]
  }

#######################
# Set up solr collection
# Create the base folder
  file { "make-spatial-folder":
    path => "/home/vagrant/solr-4.10.1/example/solr/spatial",
    ensure => directory,
    owner => "vagrant",
    group => "vagrant",
    mode => "777",
    require => File["own-solr"],
  }


# Create the conf folder
  file { "make-spatial-conf":
    path => "/home/vagrant/solr-4.10.1/example/solr/spatial/conf",
    source => "/vagrant/Assets/solr/conf",
    recurse => true,
    ensure => directory,
    owner => "vagrant",
    group => "vagrant",
    require => File["make-spatial-folder"],
  }

# Create the data folder
  file { "make-spatial-data":
    path => "/home/vagrant/solr-4.10.1/example/solr/spatial/data",
    ensure => directory,
    owner => "vagrant",
    group => "vagrant",
    mode => "777",
    require => File["make-spatial-conf"],
  }

# Create core.properties
  file { "make-core-props":
    path => "/home/vagrant/solr-4.10.1/example/solr/spatial/core.properties",
    content => "name=spatial",
    owner => "vagrant",
    group => "vagrant",
    mode => "666",
    require => File["make-spatial-data"],
  }
#######################

# Start solr running
  exec { "start-solr":
    command => "/usr/bin/java -jar /home/vagrant/solr-4.10.1/example/start.jar &",
    user => "vagrant",
    group => "vagrant",
    cwd => "/home/vagrant/solr-4.10.1/example",
    require => File["make-core-props"],
  }

  notify { 'ready':
    message  => "SOLR is now running at http://192.168.33.5:8983/solr/",
    require => Exec["start-solr"],
}